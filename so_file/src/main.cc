#include "UsbDevice.hh"
#include "extcode.h"
#include <stdio.h>
#include <string.h>
#include <usb.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>


using namespace std;

//#define VENDOR_ID   0xecd6
//#define DEVICE_ID  0x1002


extern "C" {
	    void OpenDevice(uInt32,uInt32);
	    void CloseDevice ();
	    uInt32 WriteDevice (uInt32,uInt32,uInt32 *buffer);
	    uInt32 ReadDevice (uInt32,uInt32,uInt32 *buffer);
	    uInt32 BulkRead (uInt32,uInt32,uInt32 *buffer);
	    
}

class g_USBDevice {
	public :
                UsbDevice&  dev() { return *gdev; }
                g_USBDevice() { gdev=NULL; }
                ~g_USBDevice() {}//if (gdev) delete gdev;}
                void Open (uInt32 VENDOR_ID,uInt32 DEVICE_ID) 
		{
		cout << hex << VENDOR_ID << endl;
               if (gdev) {
		        //delete gdev;
	                }
               gdev= new UsbDevice(VENDOR_ID,DEVICE_ID);
               }
                void Close () {if (gdev) 
		{
                 delete gdev;
                 }
		}
	private:
		UsbDevice *gdev;
	
} gdev;



void OpenDevice(uInt32 VENDOR_ID, uInt32 DEVICE_ID)
{
  
   gdev.Open(VENDOR_ID,DEVICE_ID);
  
}

void CloseDevice()
{
 	 gdev.Close();
}


uInt32 WriteDevice (uInt32 address,
		    uInt32 length,
		    uInt32 buffer[512])
{
  	int len;
	len = gdev.dev().write(address,length,buffer);
	//cout << len <<endl;
	 return(len/4);
}

uInt32 ReadDevice (uInt32 address,
	    	   uInt32 length,
	   	   uInt32 buffer[512])
{
 	 int len;
	len =  gdev.dev().read(address,length, buffer);
	// cout << len << endl;
		     return(len/4);
}

uInt32 BulkRead (uInt32 pipe,
		uInt32 length,
		uInt32 buffer[512])
{
	int len;
	len = gdev.dev().Read_Bulk(pipe,length,buffer);
	//cout << "bulk len " << len << endl;
	 return(len);
}

